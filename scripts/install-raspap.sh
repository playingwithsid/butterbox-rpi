set -e

# So that the admin interface loads in the user's browser-specified language
apt-get install locales-all

# Install RaspAP, accepting defaults and not installing adblock since this is
# an offline appliance.
curl -sL https://install.raspap.com | sudo bash -s -- --yes --openvpn 0 --adblock 0 --branch 2.9.8

# Configure the RaspAP network
cp /tmp/butter-setup/configs/butterbox-hostapd.conf /etc/hostapd/hostapd.conf
sed -i "s/REPLACEME/$butter_name/g" /etc/hostapd/hostapd.conf

# Configure the dnsmasq to respond at butterbox.local instead of 10.3.141.1
cp /tmp/butter-setup/configs/butterbox-dnsmasq.conf /etc/dnsmasq.d/butterbox-dnsmasq.conf
sed -i "s/REPLACEME/$butter_name/g" /etc/dnsmasq.d/butterbox-dnsmasq.conf

# Move the existing RaspAP console from the webroot to /admin
mkdir /var/www/html/admin
#mv /var/www/html/* /var/www/html/admin
mkdir /tmp/raspap-admin/
mv /var/www/html/* /tmp/raspap-admin/
mv /tmp/raspap-admin/ /var/www/html/admin

# Update lighttpd conf for RaspAP to respond at /admin instead of webroot
cp /tmp/butter-setup/configs/50-raspap-router.conf /etc/lighttpd/conf-available/50-raspap-router.conf