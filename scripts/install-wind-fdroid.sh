set -e

# Pull down a copy of the wind repository and unzip it into the expected path
curl https://guardianproject-wind.s3.amazonaws.com/repo.zip --output /tmp/fdroid-repo.zip
unzip /tmp/fdroid-repo.zip -d /tmp/fdroid-repo
# Replace the S3 hostname with our LAN address
old_host="https://guardianproject-wind.s3.amazonaws.com"
new_host="http://$butter_name.lan";
sed -i "s~$old_host~$new_host~g" /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/index.html

# used by fdroid-webdash
sed -i "s~$old_host~$new_host~g" /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/index-v2.json

# remove the QR code that points to the cloud address
rm /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/index.png
rm /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/repo/icons/icon.png

# Move the unpacked repo into the webroot, first deleting it if it already exists
if [ -d "/var/www/html/fdroid" ]; then
    rm -r "/var/www/html/fdroid"
fi
mv /tmp/fdroid-repo/guardianproject-wind.s3.amazonaws.com/fdroid/ /var/www/html

# Flutter only works on amd64 bit, which means it wont work Pi Zero W, RPi1, and RPi2
# As a workaround for 32 bit installs, consider building the app on another machine
# and transferring the static elements onto the butter box.

# Install flutter, which comes through snaps
apt-get install -y snapd
snap install flutter --classic
export PATH=$PATH:/snap/bin
/snap/bin/flutter doctor

# Clone and build webdash
git clone https://gitlab.com/uniqx/fdroid-webdash.git /tmp/fdroid-webdash
cd /tmp/fdroid-webdash
# flutter doesn't automatically appear on path until a restart.  Until then, put it on the path
/snap/bin/flutter build web --dart-define=FLUTTER_WEB_CANVASKIT_URL=/fdroid-webdash/canvaskit/ --pwa-strategy offline-first --base-href=/fdroid-webdash/ --csp
mkdir /var/www/html/fdroid-webdash
cp -r build/web/* /var/www/html/fdroid-webdash
# Webdash expects fdroid/repo to be a subpath relative to its own basepath
# We already have the files in /fdroid/repo, so make sure we also serve them from this
# new subpath
cp /tmp/butter-setup/configs/50-fdroid-webdash-repo.conf /etc/lighttpd/conf-available/
ln -s "/etc/lighttpd/conf-available/50-fdroid-webdash-repo.conf" "/etc/lighttpd/conf-enabled/50-fdroid-webdash-repo.conf"

# clean up
snap remove flutter
apt-get remove --purge snapd -y
rm -rf /snap
rm -rf /usr/local/go