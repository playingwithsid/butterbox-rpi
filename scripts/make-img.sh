# Makes an .img of this install and puts it on a USB drive, if inserted.
# Then shrinks that down so it can be copied over
# bbb = butter box backup
set -e

apt-get install pv
mkdir -p /media/bbb

# eliminate some caches
apt-get clean
rm -r /usr/local/go
rm -r /var/lib/apt/lists/*

# Assume sda since this is a fresh install with a new disk
# Note: to have this disk readable on mac and able to handle
# a 32GB image, ExFAT works well.
mount /dev/sda1 /media/bbb/
dd if=/dev/mmcblk0 bs=4M | pv | dd of=/media/bbb/bbb.img bs=4M

# Install PiShrink so we don't end up with a 32GB image
wget https://raw.githubusercontent.com/Drewsif/PiShrink/master/pishrink.sh
chmod +x pishrink.sh
mv pishrink.sh /usr/local/bin

# Shrink the image
pishrink.sh /media/bbb/bbb.img